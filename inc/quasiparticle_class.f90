module quasiparticle_class
  implicit none
  !---------------------------------------------------------------------------
  !   PARAMETERS FOR CLASS
  !---------------------------------------------------------------------------
  logical, parameter ::   &
    IS_ELECTRON = .true., &
    IS_HOLE     = .false.
  integer, parameter :: &
    q_e = -1,           &
    q_h = 1
  real, parameter :: &
    ![m^2/(V*ns)] Vals at room temp 
    !mu_e = -1.35e-4, &
    !mu_h = 4.8e-5 
    ! [mm^2/(V*ns)] T=77K
    mu_e = -2.1e-3, &
    mu_h = 1.1e-3 
  !---------------------------------------------------------------------------
  
  type Quasiparticle
    private
    integer :: q_ 
    real :: mu_ 
    real, dimension(3) :: coordinates_
  end type Quasiparticle
  
interface
  integer pure module function getCharge(this) result(ion_charge)
    type(Quasiparticle), intent(in) :: this
  end function getCharge
  
  pure module function getCoordinates(this) result(coordinates)
    type(Quasiparticle), intent(in) :: this
    real, dimension(3) :: coordinates
  end function getCoordinates
  
  pure module subroutine setCoordinates(this, new_coordinates)
    type(Quasiparticle), intent(inout) :: this
    real, dimension(3), intent(in) :: new_coordinates
  end subroutine setCoordinates

  pure module subroutine init(this, is_electron)
    type(Quasiparticle), intent(inout) :: this
    logical, intent(in) :: is_electron
  end subroutine init

  pure module function velocity(this, electric_field)
    type(Quasiparticle), intent(in) :: this
    real, dimension(3), intent(in) :: electric_field
    real, dimension(3) :: velocity
  end function velocity

  pure module subroutine move(this, velocity, time_step)
    type(Quasiparticle), intent(inout) :: this
    real, dimension(3), intent(in) :: velocity
    real, intent(in) :: time_step
  end subroutine

  real pure module function inducedCurrent(this, velocity, weighting_field) 
    type(Quasiparticle), intent(in) :: this
    real, dimension(3), intent(in) :: velocity, weighting_field
  end function inducedCurrent
end interface
end module quasiparticle_class
