module functions_mod
  implicit none

contains
  real elemental function gaussian(x, amp, mean, sigma) result(y)
    real, intent(in) :: x, amp, mean, sigma

    y = amp * exp(-(x - mean)*(x - mean)/2.0/sigma/sigma)
  end function gaussian

  real elemental function hypermet(x, amp2, mean, sigma, tau) result(y)
    real, intent(in) :: x, amp2, mean, sigma, tau
                              
    real :: s2
    s2    = sqrt(2.0)

    y = amp2*exp(((x - mean)/tau) + sigma*sigma/2.0/tau/tau)* &
      erfc(((x - mean)/s2/sigma) + sigma/s2/tau)
  end function hypermet

  real elemental function gaustail(x, amp, mean, sigma, amp2, tau) result(y)
    real, intent(in) :: x, amp, mean, sigma, amp2, tau

    y = gaussian (x, amp, mean, sigma) + hypermet (x, amp2, mean, sigma, tau)
  end function gaustail
end module functions_mod
