module grid_class
  use interpolate_mod
  implicit none
 
  !---------------------------------------------------------------------------
  !   PARAMETERS FOR CLASS
  !---------------------------------------------------------------------------
  integer, parameter ::      &
    NUM_GRID_VALS      = 8,  &! x(3), V, E(3), V0, W(3)
    NUM_VOXEL_VERTICES = 8
  integer, parameter :: &
    V_IND   = 1,        &
    E_X_IND = 2,        &
    E_Y_IND = 3,        &
    E_Z_IND = 4,        &
    V0_IND  = 5,        &
    W_X_IND = 6,        &
    W_Y_IND = 7,        &
    W_Z_IND = 8
  ! [mm]
  real, parameter ::   &
    XTAL_DEPTH = 2.0,  &
    XTAL_WIDTH = 35.0, &
    XTAL_GRID  = 0.5,  &
    ZTAL_GRID  = 0.01 
  integer, parameter ::               &
    MAX_X = XTAL_WIDTH/XTAL_GRID + 1, &
    MAX_Y = MAX_X,                    &
    MAX_Z = XTAL_DEPTH/ZTAL_GRID + 1
  !--------------------------------------------------------------------------
                    
  type :: grid
    private
    real, dimension(NUM_GRID_VALS,MAX_Z,MAX_Y,MAX_X) :: grid_
  end type grid
  
interface
  module subroutine gridRead(this, e_filename, w_filename)
    type(Grid), intent(inout) :: this
    character(len=*), intent(in) :: e_filename, w_filename
  end subroutine gridRead
  
  module subroutine gridReadMaybe(this, e_filename, w_filename)
    type(Grid), intent(inout) :: this
    character(len=*), intent(in) :: e_filename, w_filename
  end subroutine gridReadMaybe
  
  pure module function getVoxelVertices(point) result(vertices)
    real, dimension(3), intent(in) :: point 
    
    integer, dimension(3, NUM_VOXEL_VERTICES) :: vertices
  end function getVoxelVertices

  pure module function getNearestVertex(point) result(vertex)
    real, dimension(3), intent(in) :: point
    integer, dimension(3, NUM_VOXEL_VERTICES) :: vertices
    
    integer, dimension(3) :: vertex
  end function getNearestVertex

  real pure module function getValAtVertex(this, point, val_ind) result(val)
    type(Grid), intent(in) :: this
    integer, dimension(3), intent(in) :: point
    integer, intent(in) :: val_ind
  end function getValAtVertex
  
  pure module function getElectricField(this, point) result(electric_field)
    type(Grid), intent(in) :: this
    integer, dimension(3), intent(in) :: point
    
    real, dimension(3) :: electric_field
  end function getElectricField

  pure module function getWeightingField(this, point) result(weighting_field)
    type(Grid), intent(in) :: this
    integer, dimension(3), intent(in) :: point
    
    real, dimension(3) :: weighting_field
  end function getWeightingField

  pure module function getVoxelVerticesValues(this, point) result(vals)
    real, dimension(3), intent(in) :: point
    type(Grid), intent(in) :: this
    
    real, dimension(NUM_GRID_VALS, NUM_VOXEL_VERTICES) :: vals
  end function getVoxelVerticesValues
  
  real pure module function getGridValue(this, vertex, which_val) result(val)
    type(Grid), intent(in) :: this
    integer, dimension(3), intent(in) :: vertex
    integer, intent(in) :: which_val
  end function getGridValue

  real pure module function trilinearInterpolateDatum(this, vertices, point, &
      grid_datum) result(interpolated_grid_datum)
    type(Grid), intent(in) :: this
    integer, dimension(3, NUM_VOXEL_VERTICES), intent(in) :: vertices
    integer, intent(in) :: grid_datum
    real, dimension(3), intent(in) :: point ! Assume pre-adjusted
  end function trilinearInterpolateDatum
  
  pure module function nearestNeighborInterpolatedElectricField(this, point) & 
      result(electric_field)
    type(Grid), intent(in) :: this
    real, dimension(3), intent(in) :: point
    
    real, dimension(3) :: electric_field
  end function nearestNeighborInterpolatedElectricField

  pure module function trilinearInterpolatedElectricField(this, point) &
      result(electric_field)
    type(Grid), intent(in) :: this
    real, dimension(3), intent(in) :: point
    
    real, dimension(3) :: electric_field
  end function trilinearInterpolatedElectricField

  pure module function nearestNeighborInterpolatedWeightingField(this, point) & 
      result(weighting_field)
    type(Grid), intent(in) :: this
    real, dimension(3), intent(in) :: point
    
    real, dimension(3) :: weighting_field
  end function nearestNeighborInterpolatedWeightingField

  pure module function trilinearInterpolatedWeightingField(this, point) &
      result(weighting_field)
    type(Grid), intent(in) :: this
    real, dimension(3), intent(in) :: point
    
    real, dimension(3) :: weighting_field
  end function trilinearInterpolatedWeightingField
  
  pure module function geometricToIndices(this, point_geo) result(point_ind)
    real, dimension(3), intent(in) :: point_geo
    type(Grid), intent(in) :: this
    
    real, dimension(3) :: point_ind
  end function geometricToIndices
  
  pure module function geometricToTrueIndex(this, point_geo) result(point_ind)
    real, dimension(3), intent(in) :: point_geo
    type(Grid), intent(in) :: this
    
    integer, dimension(3) :: point_ind
  end function geometricToTrueIndex
  
  pure module function indicesToGeometric(this, point_ind) result(point_geo)
    integer, dimension(3), intent(in) :: point_ind
    type(Grid), intent(in) :: this
    
    real, dimension(3) :: point_geo
  end function indicesToGeometric
end interface
end module grid_class
