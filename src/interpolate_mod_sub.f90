submodule (interpolate_mod) interpolate_mod_sub
  ! Contains a few self-contained interpolations methods
  implicit none

  contains
  real pure function linearInterpolate(vals, x) result(interp_val)
    ! A simple 1D linear interpolation, assumes x\in[0,1]
    real, dimension(0:1), intent(in) :: vals
    real, intent(in) :: x
    
    interp_val = vals(0)*(1-x) + vals(1)*x
  end function linearInterpolate

  real pure function bilinearInterpolate(vals, coord) result(interp_val)
    ! A 2D (bilinear) interpolation implemented as a tensor product of two
    !   linear interpolations, assumes x,y\in[0,1]
    real, dimension(0:3), intent(in) :: vals  
    real, dimension(0:1), intent(in) :: coord
    
    interp_val = linearInterpolate([linearInterpolate(vals(0:1), coord(0)),  &
                                    linearInterpolate(vals(2:3), coord(0))], &
                                   coord(1))
  end function bilinearInterpolate

  real pure function trilinearInterpolate(vals, coord) result(interp_val)
    ! A 3D (trilinear) interpolation impletemed as a tensor product of two
    !   bilinear interpolations, assumes x,y,z\in[0,1]
    real, dimension(0:7), intent(in) :: vals
    real, dimension(0:2), intent(in) :: coord

    interp_val = linearInterpolate([bilinearInterpolate(vals(0:3), coord(0:1)),  &
                                    bilinearInterpolate(vals(4:7), coord(0:1))], &
                                   coord(2))
  end function trilinearInterpolate
end submodule interpolate_mod_sub
