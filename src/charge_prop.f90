program charge_prop
  use quasiparticle_class
  use grid_class
  use interpolate_mod
  use functions_mod
  use incidentParticle_class
  use filters_mod
  implicit none
  
  !--------------------------------------------------------------------------
  ! PARAMETERS OF PROGRAM
  !--------------------------------------------------------------------------
  ! Field file locations
  character(len=*),     parameter ::        &
    ELECTRIC_FIELD_FILENAME  = "../../comsol_stuff/comsol_grids/electric_field.csv" 
  character(len=*),     parameter ::        &
    WEIGHTING_FIELD_FILENAME = "../../comsol_stuff/comsol_grids/weighting_field.csv"
  integer,              parameter ::        &
    NUM_TIME_STEPS = 4000
  real,                 parameter ::        &
    TIME_STEP_SIZE = 0.1 ! [ns]
  integer,              parameter ::        &
    DETECTOR_BACK = 0,                      &
    DETECTOR_FRONT = 2.0 
  real, dimension(0:2), parameter ::        &
    DETECTOR_CENTER_FRONT = [0.0,0.0,2.0],  &
    DETECTOR_CENTER_CENTER = [0.0,0.0,1.0], &
    DETECTOR_CENTER_BACK = 0.0
  !--------------------------------------------------------------------------
  ! RUNTIME VARIABLES
  !--------------------------------------------------------------------------
  integer :: allocateStatus, ioStatus ! error vars
  integer :: i, j ! Loop vars
  character(len=32) :: arg ! Current command argument
  integer :: num_args, num_incident_energies
  integer :: energy_start, energy_stop, energy_step
  integer, dimension(:), allocatable :: incident_energies
  type(Grid) :: field_grid
  real, dimension(NUM_TIME_STEPS) :: current  
  !--------------------------------------------------------------------------
  
  ! One argument indicates a single energy deposition, three is some array of 
  !   values
  num_args = COMMAND_ARGUMENT_COUNT ()
  if (num_args /= 1 .and. num_args /= 3) then
    stop "Error: expect ./charge_prop energy_start energy_stop energy_step"
  else if (num_args == 1) then
    allocate (incident_energies(num_args))
    call GET_COMMAND_ARGUMENT (1, arg)
    num_incident_energies = 1
    incident_energies(1) = STOI (arg)
  else
    call GET_COMMAND_ARGUMENT (1, arg)
    energy_start = STOI (arg)
    call GET_COMMAND_ARGUMENT (2, arg)
    energy_stop = STOI (arg)
    call GET_COMMAND_ARGUMENT (3, arg)
    energy_step = STOI (arg)
    num_incident_energies = (energy_stop - energy_start)/energy_step + 1
    allocate (incident_energies(num_incident_energies))
    incident_energies = [(i, i = energy_start, energy_stop, energy_step)]
  end if
    
  ! Read in the fields from COMSOL output files
  call gridRead (field_grid, ELECTRIC_FIELD_FILENAME, WEIGHTING_FIELD_FILENAME)  

  ! Loop over each energy
  do i = 1, num_incident_energies
    current = 0.0  
    current = inducedCurrentFromIncidentParticle (field_grid, &
      DETECTOR_CENTER_FRONT, REAL (incident_energies(i)))
   
    ! "integrate"
    do j = 1, NUM_TIME_STEPS
      current(j) = current(j) + current(j-1)
    end do
    
    ! Run through electronic
    current = rc_cr2(current)
    
    ! Write the waveform
    open(unit = 20, &
      file = 'waveform_'//TRIM (STR (incident_energies(i)))//'.txt', &
      status='new', action='write')
    
    write(20, *) current
    
    close(20)

  end do

contains
  pure function inducedCurrentFromIonPair(field_grid, spawn_location) &
      result(induced_current)
    ! Intended not as a production function but to see how a single pair 
    !   induces current is created at some location
    type(Grid), intent(in) :: field_grid
    real, dimension(0:2), intent(in) :: spawn_location
    real, dimension(NUM_TIME_STEPS) :: induced_current
    induced_current = inducedCurrentFromHole (field_grid, spawn_location) +  &
      inducedCurrentFromElectron (field_grid, spawn_location)
  end function inducedCurrentFromIonPair
  
  function inducedCurrentFromIncidentParticle(field_grid, entry_location, &
      incident_energy) result(induced_current)
    ! From some incident energy and location, calculates the induced current 
    !  by each ion pair along it's deposition distribution
    type(Grid), intent(in) :: field_grid
    real, dimension(0:2), intent(in) :: entry_location
    real, intent(in) :: incident_energy
    real, dimension(NUM_TIME_STEPS) :: induced_current
    
    type(incidentParticle) :: incident_electron
    real, dimension(0:2) :: deposition_location
    real :: num_ions_at_location
    integer :: i
    
    ! Initialize the deposition distribution
    !call initParticle (incident_electron, entry_location, incident_energy)
    incident_electron = incidentParticle (entry_location, incident_energy)
    ! For each discitized bin of the trajectory
    do i = 1, NUM_DEPOSITION_BINS
      ! Get the spawn point of the pair
      ! Adjust units (um->mm) and coordinate system (z is flipped in CASINO)
      deposition_location = getDepositionLocation (incident_electron, i)/1000
      deposition_location(2) = XTAL_DEPTH - deposition_location(2)
      ! Change energy deposited to number pairs created, also keV->eV
      num_ions_at_location = getDepositionEnergy (incident_electron, i)*1000/&
        ENERGY_FOR_PAIR
      
      induced_current = induced_current + num_ions_at_location*(  &
        inducedCurrentFromHole (field_grid, deposition_location) +  &
        inducedCurrentFromElectron (field_grid, deposition_location))
    end do
  end function inducedCurrentFromIncidentParticle

  pure function inducedCurrentFromElectron(field_grid, initial_position) &
      result(induced_current)
    ! Tracks an electron through the detector given an intial_position and calcs
    !   the current at each step, return the current induced by some number of 
    !   electron spawned here.
    real, dimension(0:2), intent(in) :: initial_position
    type(Grid), intent(in) :: field_grid
    real, dimension(NUM_TIME_STEPS) :: induced_current
    type(Quasiparticle) :: electron
    real, dimension(0:2) :: electron_position, electric_field, weighting_field, &
      ion_velocity
    integer :: i

    call init (electron, IS_ELECTRON)
    call setCoordinates (electron, initial_position)
    
    i = 1
    induced_current = 0.0

    electron_position = initial_position
    do while (electron_position(2) > DETECTOR_BACK)
      electric_field  = trilinearInterpolatedElectricField (field_grid, &
        geometricToIndices (field_grid, electron_position))
      weighting_field = trilinearInterpolatedWeightingField (field_grid, &
        geometricToIndices (field_grid, electron_position))
      
      ion_velocity = velocity (electron, electric_field)

      call move (electron, ion_velocity, time_step_size)

      induced_current(i) = induced_current(i) + &
        inducedCurrent (electron, ion_velocity, weighting_field)
      electron_position = getCoordinates (electron)
      i = i + 1
      if (i == NUM_TIME_STEPS) then
        ! This is not where you want to end up, stops from SEGFaulting.
        ! Physically, this would happen with insuffiecient time steps to drift,
        !   or if a zero in the field exists
        induced_current(NUM_TIME_STEPS) = -1.0 ! Something to check against
      end if
    end do

  end function inducedCurrentFromElectron
    
  pure function inducedCurrentFromHole(field_grid, initial_position) &
    result(induced_current)
    ! Tracks an hole through the detector given an intial_position and calcs
    !   the current at each step, return the current induced by some number of 
    !   holes spawned here.
    real, dimension(0:2), intent(in) :: initial_position
    type(Grid), intent(in) :: field_grid
    real, dimension(NUM_TIME_STEPS) :: induced_current
    
    type(Quasiparticle) :: hole
    real, dimension(0:2) :: hole_position, electric_field, weighting_field, &
      ion_velocity
    integer :: i

    call init (hole, IS_HOLE)
    call setCoordinates (hole, initial_position)
    
    i = 1
    induced_current = 0.0

    hole_position = initial_position
    do while (hole_position(2) < DETECTOR_FRONT)
      electric_field  = trilinearInterpolatedElectricField (field_grid, &
        geometricToIndices (field_grid, hole_position))
      weighting_field = trilinearInterpolatedWeightingField (field_grid, &
        geometricToIndices (field_grid, hole_position))
      
      ion_velocity = velocity (hole, electric_field)

      call move (hole, ion_velocity, time_step_size)

      induced_current(i) = induced_current(i) + &
        inducedCurrent (hole, ion_velocity, weighting_field)
      hole_position = getCoordinates (hole)
      i = i + 1
      if (i == NUM_TIME_STEPS) then
        ! This is not where you want to end up, stops from SEGFaulting.
        ! Physically, this would happen with insuffiecient time steps to drift,
        !   or if a zero in the field exists
        induced_current(NUM_TIME_STEPS) = -1.0 ! Something to check against
      end if
    end do

  end function inducedCurrentFromHole
  
  character(len=20) function str(k)
    integer, intent(in) :: k
    write (str, *) k
    str = adjustl (str)
  end function str
  
  integer function stoi(s)
    character(len=*), intent(in) :: s
    read(s, *) stoi
  end function stoi
end program charge_prop
